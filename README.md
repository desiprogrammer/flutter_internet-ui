# internet_ui

A new Flutter project.

## Getting Started

### In the first page we use the following code to check for internet connection.

## We will be learning the following in this Video

    1. Checking Internet using Socket Exception.
    2. Cheking internet using Connectivity package.
    3. To not start an Application if no internet.
    4. Continuously check for internet connection.
    5. Fetch Images from URL
    6. Using Image.network 
    7. Navigator's pushreplacement

```dart
@override
  void initState(){
    super.initState();
    try {
      InternetAddress.lookup('google.com').then((result){
        if(result.isNotEmpty && result[0].rawAddress.isNotEmpty){
          // internet conn available
          // since there is an active internet connection we move to the next page which may have some widgets or some code that should be executed only if there is an INTERNET CONNECTION
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => 
            imageui(),
          ));
        }else{
          // no conn
          _showdialog();
        }
      }).catchError((error){
        // no conn
        _showdialog();
      });
    } on SocketException catch (_){
      // no internet
      _showdialog();
    }


    Connectivity().onConnectivityChanged.listen((ConnectivityResult connresult){
      if(connresult == ConnectivityResult.none){

      }else if(previous == ConnectivityResult.none){
        // internet conn
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => 
            imageui(),
          ));
      }

      previous = connresult;
    });



  }
```
