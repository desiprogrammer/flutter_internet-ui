
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:internet_ui/imageui.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "InternetUi",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.purpleAccent,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  // No need of streamsubscription
  ConnectivityResult previous;

  @override
  void initState(){
    super.initState();
    try {
      InternetAddress.lookup('google.com').then((result){
        if(result.isNotEmpty && result[0].rawAddress.isNotEmpty){
          // internet conn available
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => 
            imageui(),
          ));
        }else{
          // no conn
          _showdialog();
        }
      }).catchError((error){
        // no conn
        _showdialog();
      });
    } on SocketException catch (_){
      // no internet
      _showdialog();
    }


    Connectivity().onConnectivityChanged.listen((ConnectivityResult connresult){
      if(connresult == ConnectivityResult.none){

      }else if(previous == ConnectivityResult.none){
        // internet conn
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => 
            imageui(),
          ));
      }

      previous = connresult;
    });



  }


  void _showdialog(){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('ERROR'),
        content: Text("No Internet Detected."),
        actions: <Widget>[
          FlatButton(
            // method to exit application programitacally
            onPressed: () => SystemChannels.platform.invokeMethod('Systemnavigator.pop'),
            child: Text("Exit"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: EdgeInsets.only(
                top: 20.0
              ),
              child: Text(
                "Checking Your Internet Connection."
              ),
            ),
          ],
        ),
      ),
    );
  }
}